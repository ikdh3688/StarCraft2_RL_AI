No message yet. 

Requirements
- python2.7
- pysc2
- keras
- tensorflow==1.8
- StarCraft2 linux version

In "StarCraft2_RL_AI/Agent/A3C", 
python A3C_agent.py --map=Observer128 --render=True





Unit type mapping numbers
See s2client-api/include/sc2api/sc2_typeenums.h

REFERENCE
-----------------
[Starcraft2 Map Editor]
(http://jackmade.tistory.com/entry/4-%EC%A0%80%EC%9E%A5%ED%95%98%EA%B8%B0-%EC%8A%A4%ED%83%802-%EB%A7%B5%EC%97%90%EB%94%94%ED%84%B0-%EA%B8%B0%EC%B4%88-%EA%B0%95%EC%A2%8C?category=590325)

[puck world]
(https://cs.stanford.edu/people/karpathy/reinforcejs/puckworld.html)

[pysc2-tutorial by Steven Brown]
(https://github.com/skjb/pysc2-tutorial)

[SC2_HarvesterAgent by Alexius Meinong]
(https://github.com/CPUFronz/SC2_HarvesterAgent)
