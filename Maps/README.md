Maps
-------

How to add my own map into pysc2
-----------------------------------
We can use a custom map to train our own agent. 
First of all, we need to have your custom SC2Map in advance. 
It can be created using 'Galaxy Editor'. 

I assume that we already have a custom map and we are using StarCraftII Linux version. 

In "{python_dir}pysc2/maps" directory.
1. Define a 'map' python module which encapsulates a map list. See "KDHmaps.py". 
2. Import the map module into "__init__.py". See "__init__.py" in this directory. 
3. Copy the custom SC2Map into "StarCraftII/Maps/{Custom_map_dir}" dirctory. 
{Custom_map_dir} is the attributes that you need to assign in your map python module. See "KDHmaps.py"
4. Test: python -m pysc2.bin.play --map {your_custom_map_name}



REFERENCE
-------------
https://github.com/deepmind/pysc2/blob/master/docs/maps.md
