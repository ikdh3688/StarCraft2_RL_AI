"""
/*
* =====================================================================================
*
*       Filename:  A3CAgent.py
*
*    Description:  This is RL agent script for learning how to play Starcraft2 game.
*
*        Version:  0.3
*        Created:  06/05/18
*       Revision:  none
*       Compiler:  python2.7
*
*         Author:  Do Hyung Kim, Jin Won Lee
*   Organization:  UNIST
*
* =====================================================================================
*/
"""


from keras import backend as K
from keras.models import Model
import keras.layers
from keras.layers import Dense, Conv2D, Flatten, Input, Add
from keras.layers.normalization import BatchNormalization
from keras.optimizers import RMSprop

from pysc2.env import sc2_env 
from pysc2.env import run_loop
from pysc2.lib import actions
from pysc2.lib import features
from pysc2.agents import base_agent

from absl import app
from absl import flags

import numpy as np
import threading
import time
import random
import tensorflow as tf

from math import hypot

GLOBAL_EPISODE=0
GLOBAL_STEPS=0

# Env Setting
_MAP_NAME = "Observer72"
_SCREEN_SIZE_X = 64             # TODO (dohyung) : screen_px params in pysc2 are deprecated. Modify this using AgentInterfaceFormat().
_SCREEN_SIZE_Y = 64
_MINIMAP_SIZE_X = 64            # TODO (dohyung) : minimap_px params in pysc2 are deprecated. Modify this using AgentInterfaceFormat().
_MINIMAP_SIZE_Y = 64
_STEP_MUL = 4
_T_MAX = 200
_NUM_THREADS = 1
_RENDER = False
_SAVE_REPLAY = False
_LOAD_MODEL = False
_MAX_AGENT_STEPS = 0

# Flags
FLAGS = flags.FLAGS
flags.DEFINE_bool("render", False, "Whether to render with pygame.")
flags.DEFINE_integer("screen_resolution", 64,
                     "Resolution for screen feature layers.")
flags.DEFINE_integer("minimap_resolution", 64,
                     "Resolution for minimap feature layers.")
flags.DEFINE_integer("max_agent_steps", 0, "Total agent steps. Default is 0 which is unlimited.")
flags.DEFINE_integer("t_max", 200, "Agent's maximum time step for one episode.")
flags.DEFINE_integer("step_mul", 4, "Game steps per agent step.")
flags.DEFINE_integer("num_threads", 1, "How many instances to run in parallel.")
flags.DEFINE_bool("save_replay", False, "Whether to save a replay at the end.")
flags.DEFINE_bool("load_model", False, "Whether to load neural network model weights. It needs .h5 file.")
flags.DEFINE_string("map", None, "Name of a map to use.")
flags.mark_flag_as_required("map")

# State 
_SPATIAL_STATE_SIZE = (_SCREEN_SIZE_X, _SCREEN_SIZE_Y, 3)
_NONSPATIAL_STATE_SIZE = 4 + 16           # 16 is reserved in future.
_ACTION_SIZE_X=20
_ACTION_SIZE_Y=20

# Functions
_NOOP = actions.FUNCTIONS.no_op.id
_MOVE_CAMERA = actions.FUNCTIONS.move_camera.id
_MOVE_SCREEN = actions.FUNCTIONS.Move_screen.id
_SELECT_POINT = actions.FUNCTIONS.select_point.id

# Features
_SCREEN_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
_SCREEN_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
_SCREEN_UNIT_DENSITY= features.SCREEN_FEATURES.unit_density.index
_MINIMAP_PLAYER_ID = features.MINIMAP_FEATURES.player_id.index
_MINIMAP_PLAYER_RELATIVE = features.MINIMAP_FEATURES.player_relative.index

# Parameters
_PLAYER_SELF = 1
_PLAYER_ALLY = 2
_PLAYER_NEUTRAL = 3
_PLAYER_HOSTILE = 4
_NOT_QUEUED = [0]
_QUEUED = [1]

# Unit IDs
_PROTOSS_PROBE = 84
_PROTOSS_OBSERVER = 82
_PROTOSS_PHOENIX = 78
_PROTOSS_STALKER = 74           # SMART, MOVE, PATROL, HOLDPOSITION, STOP, RALLY_UNITS, ATTACK, EFFECT_BLINK
_TERRAN_MARINE = 48             # SMART, MOVE, PATROL, HOLDPOSITION, STOP, ATTACK, EFFECT_STIM
_TERRAN_REAPER = 49             # SMART, MOVE, PATROL, HOLDPOSITION, EFFECT_KD8CHARGE, STOP, ATTACK

# Effect IDs
_EFFECT_STIM_MARINE = 380       # Target: None.
_EFFECT_BLINK = 3687            # Target: Point.
_EFFECT_BLINK_STALKER = 1442    # Target: Point.




# Agent for managing a global neural network.
class A3CAgent():

    def __init__(self):
        # Env setting
        self.map_name = _MAP_NAME
        self.render = _RENDER
        self.save_replay = _SAVE_REPLAY
       
        # States definition
        self.spatial_state_size = _SPATIAL_STATE_SIZE 
        self.nonspatial_state_size = _NONSPATIAL_STATE_SIZE 
        self.action_size = _ACTION_SIZE_X *_ACTION_SIZE_Y # 20-by-20
            
        # A3C Hyperparameters 
        self.discount_factor = 0.95
        self.actor_lr = 1e-5
        self.critic_lr= 1e-5
        
        # Multi threading
        self.threads = _NUM_THREADS
        self.lock = threading.Lock()

        # Neural Networks
        self.actor, self.critic = self.build_model()
        self.optimizer = [self.actor_optimizer(), self.critic_optimizer()]
        self.load_nn_model = _LOAD_MODEL
        

        # Session for summary
        self.sess = tf.InteractiveSession()
        K.set_session(self.sess)

        if self.load_nn_model:
            self.load_model("./save_model/sc2_obs_a3c")
        else:
            self.sess.run(tf.global_variables_initializer())

        # summary
        self.summary_placeholders, self.update_ops, self.summary_op = \
                self.setup_summary()
        self.summary_writer = tf.summary.FileWriter('summary/SC2_a3c', self.sess.graph)

        # DEBUG
        self.DEBUG = open("./log", 'w')

    
    def train(self):
        # DONE. (dohyung) : Add argument in Agent()
        agents = [Agent(self.action_size, [self.spatial_state_size, self.nonspatial_state_size], 
            [self.actor, self.critic], self.sess, self.optimizer, self.discount_factor, 
            [self.summary_op, self.summary_placeholders, self.update_ops, self.summary_writer],
            thread_id=i, lock=self.lock) for i in range(self.threads)]
    
        def run_thread(agent):
            """Run an agent"""
            with sc2_env.SC2Env(
                map_name=self.map_name,
                step_mul=_STEP_MUL,
                screen_size_px=(_SCREEN_SIZE_X, _SCREEN_SIZE_Y),
                minimap_size_px=(_MINIMAP_SIZE_X, _MINIMAP_SIZE_Y),
                game_steps_per_episode=200*_STEP_MUL,
                visualize=self.render) as env:

                if _MAX_AGENT_STEPS == 0:
                    run_loop.run_loop([agent], env)
                else:
                    run_loop.run_loop([agent], env, _MAX_AGENT_STEPS)

                if self.save_replay:
                    env.save_replay("A3C_agent_%s" % self.map_name)

        threads = []
        for agent in agents:
            time.sleep(1)
            t = threading.Thread(target=run_thread, args=[agent])
            threads.append(t)
            t.start()
        
        # Save global network model.
        while True:
            time.sleep(60 * 10)
            self.save_model("./save_model/sc2_obs_a3c")
            # For training info
            global GLOBAL_EPISODE, GLOBAL_STEPS
            self.DEBUG.write("[%s] GLOBAL_EPISODE: %d GLOBAL_STEPS: %d" % (time.ctime(), GLOBAL_EPISODE, GLOBAL_STEPS))

        # Wait 
        #for t in threads:
        #    t.join()


    def build_model(self):
        # TODO (dohyung) : Modify to use conv layer for spatial inputs 
        # and to use fc layer for non-spatial layer

        # Convolutional layer for spatial inputs
        input_spatial = Input(shape=self.spatial_state_size, name='input_spatial')
        conv = Conv2D(32, (8, 8), strides=(4, 4), activation='relu', name='conv2d_spatial_1')(input_spatial)
        conv = BatchNormalization()(conv)
        conv = Conv2D(64, (4, 4), strides=(2, 2), activation='relu', name='conv2d_spatial_2')(conv)
        conv = BatchNormalization()(conv)
        conv = Flatten()(conv)

        # Fully-connected layer for non-spatial inputs
        input_nonspatial = Input(shape=(self.nonspatial_state_size, ), name='input_nonspatial')
        fc = Dense(128, activation='relu', name='fc_nonspatial_1')(input_nonspatial)
        fc = BatchNormalization()(fc)
        fc = Dense(256, activation='relu', name='fc_nonspatial_2')(fc)
        fc = BatchNormalization()(fc)
        
        # Merge two layers
        merged_fc = keras.layers.concatenate([conv, fc])
        merged_fc = Dense(1024, activation='relu', name='merged_fc_1')(merged_fc)
        merged_fc = Dense(2024, activation='relu', name='merged_fc_2')(merged_fc)
        merged_fc = BatchNormalization()(merged_fc)

        policy = Dense(self.action_size, activation='softmax', name='policy_output')(merged_fc)
        value = Dense(1, activation='linear', name='value_output')(merged_fc)
        
        actor = Model(inputs=[input_spatial, input_nonspatial], outputs=policy)
        critic = Model(inputs=[input_spatial, input_nonspatial], outputs=value)

        actor._make_predict_function()
        critic._make_predict_function()
        
        actor.summary()
        critic.summary()
        
        return actor, critic
        
    def actor_optimizer(self):
        action = K.placeholder(shape=[None, self.action_size])
        advantages = K.placeholder(shape=[None,])

        policy = self.actor.output

        # loss function : cross entropy
        action_prob = K.sum(action * policy, axis=1)
        cross_entropy = K.log(action_prob + 1e-10) * advantages
        cross_entropy = -K.sum(cross_entropy)

        entropy = K.sum(policy * K.log(policy + 1e-10), axis=1)
        entropy = K.sum(entropy)

        loss = cross_entropy + 0.01 * entropy 

        optimizer = RMSprop(lr=self.actor_lr, rho=0.99, epsilon=0.01)
        updates = optimizer.get_updates(self.actor.trainable_weights, [], loss)
        train = K.function([self.actor.input[0], self.actor.input[1], action, advantages], [loss], updates=updates)

        return train
    
    def critic_optimizer(self):
        discounted_prediction = K.placeholder(shape=(None,))

        value = self.critic.output

        loss = K.mean(K.square(discounted_prediction - value))
        loss = K.log(loss)

        optimizer = RMSprop(lr=self.critic_lr, rho=0.99, epsilon=0.01)
        updates = optimizer.get_updates(self.critic.trainable_weights, [], loss)
        train = K.function([self.critic.input[0],self.critic.input[1], discounted_prediction], [loss], updates=updates)
    
        return train

    def load_model(self, name):
        self.actor.load_weights(name + "_actor.h5")
        self.critic.load_weights(name + "_critic.h5")
        print("="*10, "[KDH] Successfully actor-critic models are loaded." , "="*10)
    
    def save_model(self, name):
        self.actor.save_weights(name + "_actor.h5")
        self.critic.save_weights(name + "_critic.h5")

    def setup_summary(self):
        episode_total_reward = tf.Variable(0.)
        episode_avg_max_q = tf.Variable(0.)
        episode_duration = tf.Variable(0.)
        episode_avg_actor_loss = tf.Variable(0.)
        episode_avg_critic_loss = tf.Variable(0.)
        
        tf.summary.scalar('Total Reward/Episode', episode_total_reward)
        tf.summary.scalar('Average Max Prob/Episode', episode_avg_max_q)
        tf.summary.scalar('Duration/Episode', episode_duration)
        tf.summary.scalar('Average policy_network loss/Episode', episode_avg_actor_loss)
        tf.summary.scalar('Average value_network loss/Episode', episode_avg_critic_loss)

        summary_vars = [episode_total_reward,
                        episode_avg_max_q,
                        episode_duration,
                        episode_avg_actor_loss,
                        episode_avg_critic_loss]

        summary_placeholders = [tf.placeholder(tf.float32) for _ in range(len(summary_vars))]
        update_ops = [summary_vars[i].assign(summary_placeholders[i]) for i in range(len(summary_vars))]
        summary_op = tf.summary.merge_all()

        return summary_placeholders, update_ops, summary_op




# It follows base agent interface in pysc2.agent dir.
class Agent(base_agent.BaseAgent):
    def __init__(self, action_size, state_size, model, sess,
                optimizer, discount_factor, summary_ops, thread_id=None, lock=None):
        # SC2 env parameters .
        self.reward = 0
        self.episodes = 0
        self.steps = 0
        self.obs_spec = None
        self.action_spec = None
        self.thread_id = thread_id
        self.lock = lock
        self.epsilon = 0.99999
        self.epsilon_decay = 0.999999

        #self.epsilon = 0.999
        #self.epsilon = 0.05
        #self.epsilon_decay = 0.9999

        # Actor-critic parameters.
        self.action_size = action_size
        [self.spatial_state_size, self.nonspatial_state_size] = state_size
        self.actor, self.critic = model
        self.sess = sess
        self.optimizer = optimizer # len(optimizer) = 2
        self.discount_factor = discount_factor
        [self.summary_op, self.summary_placeholders, 
        self.update_ops, self.summary_writer] = summary_ops

        # Local models.
        self.local_actor, self.local_critic = self.build_local_model()

        # Multiple steps for samples.
        self.t_max = _T_MAX
        self.t = 0
        
        # 
        self.previous_reward = np.inf
        self.history = np.array([])
        self.state = np.array([])

        # Lists for samples.
        self.states, self.actions, self.rewards = [], [], []

        # Target, agent location
        # TODO (dohyung) : Remove hardcoding.
        self.target_location = [_MINIMAP_SIZE_X / 2 - 5, 15] 
        self.agent_location_screen = None
        self.agent_location_minimap = None

        # Summary 
        self.score = 0.0
        self.avg_p_max = 0.0
        self.avg_actor_loss = 0.0
        self.avg_critic_loss = 0.0


    def setup(self, obs_spec, action_spec):
        self.obs_spec = obs_spec
        self.action_spec = action_spec
        print("[KDH] setup..")


    def reset(self):
        # DONE. (dohyung) : Write summary and print some logs.
        # DONE. (dohyung) : Update the global networks using samples .
        # DONE. (dohyung) : Set the weights of the local networks using those of the global networks.
        self.episodes += 1
        global GLOBAL_EPISODE, GLOBAL_STEPS
        with self.lock:
            GLOBAL_EPISODE += 1
            GLOBAL_STEPS += self.t

        if self.t == 0:
            return 

        dead = True if self.t < self.t_max else False
        print("[KDH] t", self.t)

        self.train_model(dead)
        self.update_local_model()
        
        # Write summaries
        stats = [self.score, self.avg_p_max / float(self.t), self.t, 
                self.avg_actor_loss / float(self.t), self.avg_critic_loss / float(self.t)]

        for i in range(len(stats)):
            self.sess.run(self.update_ops[i], 
                    feed_dict={self.summary_placeholders[i]: float(stats[i])})
            

        summary_str = self.sess.run(self.summary_op)
        self.summary_writer.add_summary(summary_str, self.episodes + 1)

        self.t = 0
        self.score = 0
        self.avg_p_max = 0
        self.avg_actor_loss = 0
        self.avg_critic_loss = 0
    

    def step(self, obs):
        self.steps += 1
        self.t += 1

        self.agent_location_screen = self.find_agent_location_in_screen(obs)
        self.agent_location_minimap = self.find_agent_location_in_minimap(obs)

        # DONE (dohyung) : Select the agent unit at the initial state. 
        if obs.first():
            player_relative = obs.observation["screen"][_SCREEN_PLAYER_RELATIVE]
            self.history = np.stack([player_relative, player_relative] , axis=2)
            return actions.FunctionCall(_SELECT_POINT, [_NOT_QUEUED, self.agent_location_screen])

        if obs.last():
            self.final_reward = obs.observation["score_cumulative"][0]  # final_score  1 or -1
            print("[KDH%d] Final Score at the end of episode:" % self.t, self.final_reward)
            return 

        #time.sleep(0.3)
        # DONE. (dohyung) : Implement screen_move action function.
        if self.t % 3 == 0:
            return actions.FunctionCall(_MOVE_CAMERA, [self.agent_location_minimap])

        #np.set_printoptions(threshold='nan')
        #from pdb import set_trace
        #set_trace()

        # Get state, action, reward
        state = self.transform_obs_to_state(obs)
        action = self.get_action(state)
        reward = self.get_reward(obs)
        print("[KDH] reward : ", reward) 

        # Append sample
        self.history = np.reshape([state[0]], _SPATIAL_STATE_SIZE)
        self.append_sample(state, action, reward)

        self.avg_p_max += np.amax(self.actor.predict(state))
        self.score += reward

        # 10-by-10 matrix 
        target = self.transform_action_to_point(action)
        print("[KDH] target location: ", target)
        return actions.FunctionCall(_MOVE_SCREEN, [_NOT_QUEUED, target])
    
    # DONE (dohyung) : Implement transforming action function.
    def transform_action_to_point(self, action_index):
        # The center point is the location of my agent unit.
        # Calculate the next point after the next movement.
        width, height = _ACTION_SIZE_X, _ACTION_SIZE_Y

        loc_x, loc_y = int(action_index % width), int(action_index / height)
        target_x = loc_x - (width/2) + self.agent_location_screen[0] 
        target_y = loc_y - (height/2) + self.agent_location_screen[1] 

        target_x = np.clip(target_x, 0, _SCREEN_SIZE_X - 1)
        target_y = np.clip(target_y, 0, _SCREEN_SIZE_Y - 1)
        return [target_x, target_y]

    # DONE (jwl1993) : Implement transform_obs_to_state
    def transform_obs_to_state(self, obs):

        cur_image = obs.observation["screen"][_SCREEN_PLAYER_RELATIVE]
        #location_agent_screen = (obs.observation["screen"][_SCREEN_PLAYER_RELATIVE] == _PLAYER_SELF).astype(int)
        location_agent_screen = (obs.observation["minimap"][_SCREEN_PLAYER_RELATIVE] == _PLAYER_SELF).astype(int)
        len_ns = 20
        len_s = 3
        unit_type = _PROTOSS_PHOENIX 
        screen_agent_x, screen_agent_y = self.find_agent_location_in_screen(obs)
        speed = 0

        non_spatial_data = np.array([])
        spatial_data = np.array([])

        #np.set_printoptions(threshold='nan')
        #from pdb import set_trace
        #set_trace()

        #self.history = np.stack([self.history[:,:,1:3], cur_image], axis=2)
        cur_image = np.reshape(cur_image, (_SPATIAL_STATE_SIZE[0], _SPATIAL_STATE_SIZE[1], 1))
        location_agent_screen = np.reshape(location_agent_screen, (_SPATIAL_STATE_SIZE[0], _SPATIAL_STATE_SIZE[1], 1))

        self.history = np.append(cur_image, self.history[:,:,0:1], axis=2)
        self.history = np.append(self.history, location_agent_screen, axis=2)
        #print(self.history)

        image_0_y, image_0_x = (self.history[:,:,0] == _PLAYER_SELF).nonzero()
        image_1_y, image_1_x = (self.history[:,:,1] == _PLAYER_SELF).nonzero()

        dist = hypot((image_1_x[0] - image_0_x[0]), (image_1_y[0] - image_0_y[0]))
        speed = (float)(dist) / (float)(_STEP_MUL)

        spatial_data = np.copy(self.history)    #Even history modify after this, spatial_data do not updated

        non_spatial_data = np.append(non_spatial_data, [unit_type])
        #non_spatial_data = np.append(non_spatial_data, [screen_agent_x])
        #non_spatial_data = np.append(non_spatial_data, [screen_agent_y])
        non_spatial_data = np.append(non_spatial_data, [speed])

        while len(spatial_data[0,0,]) != len_s:
            spatial_data = np.append(spatial_data, np.zeros((64,64,1)), axis=2)
        while len(non_spatial_data) != len_ns:
           non_spatial_data = np.append(non_spatial_data, [0])
        
        # Reshape
        spatial_data = np.reshape([spatial_data], (1, _SPATIAL_STATE_SIZE[0], _SPATIAL_STATE_SIZE[1], _SPATIAL_STATE_SIZE[2]))
        non_spatial_data = np.reshape([non_spatial_data], (1, len(non_spatial_data)))
        return [spatial_data, non_spatial_data]

    # DONE (jwl1993) : Implement.
    def get_reward(self, obs):
        reward = 0
        #dist_threshold = 10
        #n_observer_in_b = 0.0
        dest_x, dest_y = self.target_location 
        coord_x, coord_y = self.find_agent_location_in_minimap(obs)
        coord_x_, coord_y_ = self.find_agent_location_in_screen(obs)
        
        #unit_type = obs.observation["screen"][_SCREEN_UNIT_TYPE]
        #unit_density = obs.observation["screen"][_SCREEN_UNIT_DENSITY]

        #unit_y, unit_x = (unit_type == _PROTOSS_OBSERVER).nonzero()

        #for obs_x, obs_y in zip(unit_x, unit_y):
        #    dens = unit_density[obs_y][obs_x] 
        #    tmp_dist = hypot((obs_x - coord_x_),(obs_y - coord_y_))
        #    if tmp_dist < dist_threshold:
        #        n_observer_in_b += 1 * dens

        #n_observer_in_b /= 9
        
        dist = hypot((dest_x - coord_x), (dest_y - coord_y))
            

        #reward = -1 *(dist + n_observer_in_b)
        # TODO (dohyung) : We need to tune these rewards properly to avoid stucking in local minima.
        #reward = -1 *(dist + n_observer_in_b/5)
        reward = -1 *(dist)
        previous_reward = self.previous_reward
        if self.previous_reward > 0:
            self.previous_reward = reward
            reward = 0
        else:
            self.previous_reward = reward
            reward = reward - previous_reward

        return reward

    def find_agent_location_in_minimap(self, obs):
        player_relative = obs.observation["minimap"][_MINIMAP_PLAYER_RELATIVE]
        agent_y, agent_x = (player_relative == _PLAYER_SELF).nonzero()
        return [agent_x[0], agent_y[0]]

    def find_agent_location_in_screen(self, obs):
        player_relative = obs.observation["screen"][_SCREEN_PLAYER_RELATIVE]
        agent_y, agent_x = (player_relative == _PLAYER_SELF).nonzero()
        return [agent_x[0], agent_y[0]]

    
    def build_local_model(self):
        # DONE. (dohyung) : Modify to use conv layer for spatial inputs 
        # and to use fc layer for non-spatial layer

        # Convolutional layer for spatial inputs
        input_spatial = Input(shape=self.spatial_state_size, name='input_spatial')
        conv = Conv2D(32, (8, 8), strides=(4, 4), activation='relu', name='conv2d_spatial_1')(input_spatial)
        conv = BatchNormalization()(conv)
        conv = Conv2D(64, (4, 4), strides=(2, 2), activation='relu', name='conv2d_spatial_2')(conv)
        conv = BatchNormalization()(conv)
        conv = Flatten()(conv)

        # Fully-connected layer for non-spatial inputs
        input_nonspatial = Input(shape=(self.nonspatial_state_size, ), name='input_nonspatial')
        fc = Dense(128, activation='relu', name='fc_nonspatial_1')(input_nonspatial)
        fc = BatchNormalization()(fc)
        fc = Dense(256, activation='relu', name='fc_nonspatial_2')(fc)
        fc = BatchNormalization()(fc)
        
        # Merge two layers
        #merged_fc = Add()([conv, fc])
        merged_fc = keras.layers.concatenate([conv, fc])
        merged_fc = Dense(1024, activation='relu', name='merged_fc_1')(merged_fc)
        merged_fc = Dense(2024, activation='relu', name='merged_fc_2')(merged_fc)
        merged_fc = BatchNormalization()(merged_fc)

        policy = Dense(self.action_size, activation='softmax', name='policy_output')(merged_fc)
        value = Dense(1, activation='linear', name='value_output')(merged_fc)
        
        local_actor = Model(inputs=[input_spatial, input_nonspatial], outputs=policy)
        local_critic = Model(inputs=[input_spatial, input_nonspatial], outputs=value)

        local_actor._make_predict_function()
        local_critic._make_predict_function()
        
        # This part is only a diffenrence compared to building global model.
        local_actor.set_weights(self.actor.get_weights())
        local_critic.set_weights(self.critic.get_weights())
        
        local_actor.summary()
        local_critic.summary()
        
        return local_actor, local_critic
    
    # DONE. (dohyung) : Implement 
    def train_model(self, done=False):
        discounted_rewards = self.discounted_rewards(self.rewards, done)
        
        spatial_states = np.zeros((len(self.states), _SPATIAL_STATE_SIZE[0], _SPATIAL_STATE_SIZE[1], _SPATIAL_STATE_SIZE[2]))
        nonspatial_states = np.zeros((len(self.states), _NONSPATIAL_STATE_SIZE))
        assert len(spatial_states) == len(nonspatial_states)

        for i in range(len(self.states)):
            spatial_states[i] = self.states[i][0]
            nonspatial_states[i] = self.states[i][1]
            #states[i][0] = spatial_states[i]
            #states[i][1] = nonspatial_states[i]
        states = [spatial_states, nonspatial_states]
        

        values = self.local_critic.predict(states)
        values = np.reshape(values, len(values))
        advantages = discounted_rewards - values

        self.avg_actor_loss += self.optimizer[0]([states[0], states[1], self.actions, advantages])[0]
        print("[KDH] avg_actor_loss:", self.avg_actor_loss)
        self.avg_critic_loss += self.optimizer[1]([states[0], states[1], discounted_rewards])[0]
        print("[KDH] avg_critic_loss:", self.avg_critic_loss)
        self.states, self.actions, self.rewards = [], [], []

    # DONE (dohyung) : Implement 
    def discounted_rewards(self, rewards, done=False):
        discounted_rewards = np.zeros_like(rewards) # same shape
        R = 0
        if not done:
            R = self.critic.predict(np.float32(self.states[-1]))[0]
        else:
            R = self.final_reward 

        for t in reversed(range(0, len(rewards))):
            R = R * self.discount_factor + rewards[t]
            discounted_rewards[t] = R
        return discounted_rewards

    # DONE (dohyung) : Implement 
    def update_local_model(self):
        self.local_actor.set_weights(self.actor.get_weights())
        self.local_critic.set_weights(self.critic.get_weights())
    
    # DONE (dohyung) : Implement 
    def get_action(self, state):
        # Exploration
        random_number = np.random.rand()
        if random_number < self.epsilon:
            print("[KDH] self.epsilon : ", self.epsilon) 
            action_index = np.random.randint(0, self.action_size - 1, size=1)[0]
            #self.epsilon *= self.epsilon_decay
            self.epsilon = max(0.05, self.epsilon * self.epsilon_decay)
        else:
            policy = self.local_actor.predict(state)[0]
            #np.set_printoptions(threshold='nan')
            #from pdb import set_trace
            #set_trace()
            #print(policy)
            action_index = np.random.choice(self.action_size, 1, p=policy)[0]
        return action_index

    # DONE (dohyung) : Implement 
    def append_sample(self, state, action, reward):
        self.states.append(state) # state : list[spatial_state, nonspatial_state] 
        act = np.zeros(self.action_size)
        act[action] = 1
        self.actions.append(act)
        self.rewards.append(reward)


def print_params():
    print("="*10, "parameter info", "="*10)
    print("[KDH]  _MAP_NAME :", _MAP_NAME)
    print("[KDH]  _SCREEN_SIZE_X :", _SCREEN_SIZE_X)
    print("[KDH]  _SCREEN_SIZE_Y :", _SCREEN_SIZE_Y)
    print("[KDH]  _MINIMAP_SIZE_X :", _MINIMAP_SIZE_X)
    print("[KDH]  _MINIMAP_SIZE_Y :", _MINIMAP_SIZE_Y)
    print("[KDH]  _STEP_MUL :", _STEP_MUL)
    print("[KDH]  _T_MAX :", _T_MAX)
    print("[KDH]  _NUM_THREADS :", _NUM_THREADS)
    print("[KDH]  _SAVE_REPLAY :", _SAVE_REPLAY)
    print("[KDH]  _RENDER :", _RENDER)
    print("[KDH]  _LOAD_MOEL :", _LOAD_MODEL)
    print("="*20)

def set_params():
    # Setting environment variables
    global _SCREEN_SIZE_X, _SCREEN_SIZE_Y, _MINIMAP_SIZE_X,  \
    _MINIMAP_SIZE_Y, _STEP_MUL, _T_MAX, _NUM_THREADS, _SAVE_REPLAY,  \
    _MAP_NAME, _RENDER, _LOAD_MODEL, _MAX_AGENT_STEPS
    _SCREEN_SIZE_X = FLAGS.screen_resolution
    _SCREEN_SIZE_Y = FLAGS.screen_resolution
    _MINIMAP_SIZE_X = FLAGS.minimap_resolution
    _MINIMAP_SIZE_Y = FLAGS.minimap_resolution
    _STEP_MUL = FLAGS.step_mul
    _T_MAX = FLAGS.t_max
    _MAX_AGENT_STEPS = FLAGS.max_agent_steps
    _NUM_THREADS = FLAGS.num_threads
    _SAVE_REPLAY = FLAGS.save_replay
    _MAP_NAME = FLAGS.map
    _RENDER = FLAGS.render
    _LOAD_MODEL = FLAGS.load_model

def main(argv):
    set_params()
    print_params()

    #agent = A3CAgent(map_name="Observer72", render=False, save_replay=False, num_threads=10, load_nn_model=True)
    #agent = A3CAgent(map_name="Observer72", render=True, save_replay=False, num_threads=1, load_nn_model=True)
    agent = A3CAgent()
    agent.train()

if __name__ == "__main__":
    print("Starting...")
    app.run(main)

