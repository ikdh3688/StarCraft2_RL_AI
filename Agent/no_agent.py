from pysc2.agents import base_agent
from pysc2.lib import actions
from pysc2.lib import features

import time
import random 

# Functions
_BUILD_SUPPLYDEPOT = actions.FUNCTIONS.Build_SupplyDepot_screen.id
_BUILD_BARRACKS = actions.FUNCTIONS.Build_Barracks_screen.id
_NOOP = actions.FUNCTIONS.no_op.id
_SELECT_POINT = actions.FUNCTIONS.select_point.id
_MOVE_CAMERA = actions.FUNCTIONS.move_camera.id
_MOVE_SCREEN = actions.FUNCTIONS.Move_screen.id

# Features
_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index

# Unit IDs
_TERRAN_COMMANDCENTER = 18
_TERRAN_SCV = 45

# Parameters
_PLAYER_SELF = 1
_NOT_QUEUED = [0]
_QUEUED = [1]




class NoAgent(base_agent.BaseAgent):
    base_top_left = None 
    supply_depot_built = False
    barracks_built = False
    scv_move = False
    scv_selected = False

    boundary = 20 

    def step(self, obs):
        super(NoAgent, self).step(obs)

        time.sleep(0.5)


        if not self.scv_selected:
            unit_type = obs.observation["screen"][_UNIT_TYPE]
            #print("[KDH] (unit_type == _TERRAN_SCV) ", (unit_type == _TERRAN_SCV))
            #print("[KDH] (unit_type == _TERRAN_SCV).nonzero() ", (unit_type == _TERRAN_SCV).nonzero())
            unit_y, unit_x = (unit_type == _TERRAN_SCV).nonzero() #numpy array <-> boolean variable

            #print("[KDH] unit_x, unit_y", unit_x, unit_y) 
            target = [unit_x[0], unit_y[0]]

            self.scv_selected = True
            # FunctionCall(__FUNC_ID__, __LIST_OF_ARGUMENTS__)
            return actions.FunctionCall(_SELECT_POINT, [_NOT_QUEUED, target])
        else:
            if _MOVE_SCREEN in obs.observation["available_actions"]:
                unit_type = obs.observation["screen"][_UNIT_TYPE]
                unit_y, unit_x = (unit_type == _TERRAN_COMMANDCENTER).nonzero()

                # Target point to build a supply depot.
                rand_x = random.randrange(-1*self.boundary,self.boundary)
                rand_y = random.randrange(-1*self.boundary,self.boundary)
                target = self.transformLocation(int(unit_x.mean()), rand_x, int(unit_y.mean()), rand_y)
                return actions.FunctionCall(_MOVE_SCREEN, [_NOT_QUEUED, target])
       

        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def transformLocation(self, x, x_distance, y, y_distance):
        return [x + x_distance, y + y_distance]


    
